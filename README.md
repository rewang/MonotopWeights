MonotopWeights
====================

ResWjj
-----------------------------------
- base sample: `MGPy8EG_A14N23LO_MonotopDMF_ResWjj_mMed5000_mX10`


NonresWjj
-----------------------------------
- base sample: `MGPy8EG_A14N23LO_MonotopDMF_NonresWjj_mMed3000_mX1`


NonresWlv
-----------------------------------
- base sample: `mc15_13TeV.302776.MGPy8_MonotopNRl_v2500a0p2.*`



How to apply the DM weights: 
-----------------------------------
```sh
TFile *DMWeights_File = NULL;
TH1F* h_DMweights = NULL;
DMWeights_File = TFile::Open(genwimpweights.Data());
h_DMweights = (TH1F *) DMWeights_File->Get("pt_chichi");
```
the weight is added event by event
```sh
weight *= getSFfrom1DHist( truthMET, h_DMweights);
````
Here `truthMET` comes from the vector sum of two DM particles. In the case of leptonic channel, the neutrino from W decay is excluded from this variable. 
In the case of resonant model, there is only ONE DM particle. 
```sh
DMWeights_File->Close();
```